# Pomodoro

![Screenshot of APP](pomodoro.png)

## What Do?

I suck at studying, so instead of studying I made a program to help me study!
It's a simple pomodoro timer written in C. It's missing a bunch of features 
which I may or may not build in later. Currently I only support linux and unix 
based systems as that is what I generally work with. However, it shouldn't be
too difficult to compile it for an NT based system.


## Stuff I have to do

- [ ] [Add audio chime support]
- [ ] [Add notification daemon support]
- [ ] [Fix Pause issue requiring multi-click]

## The program keeps crashing due to unicode support

If this happens to you, it means that your ncurses library isn't compiled with
the unicode use flags enabled. Make sure to recompile with these flags and the
code should compile and run just fine.

## Other Issues

If you run into any weird issues with the app or code (that isn't already
mentioned above), please file an issue or send me an
[email](mailto:contact@i330.dev).
