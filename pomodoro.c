#include<ncurses.h>
#include<stdlib.h>
#include<locale.h>
#include<string.h>
#include<unistd.h>
#include<math.h>

//--[I-330 : 2024]----------------------------------------
// I intend to re-factor a lot of this code and design a 
// more intuitive UI some time this year. I have learned 
// a lot about UI and code design in the past couple years
// that I really want to apply to this application. There 
// are very few good pomodoro apps for unix/linux, and I 
// would like to create something useful.
//--------------------------------------------------------

// Method prints START button, changes highlight depending on cursRow Value
void timerButton(unsigned int cursRow){
  if(cursRow==3){attrset(A_BOLD | A_REVERSE);}else{attrset(A_NORMAL);}
  mvaddstr(20,5,"╔═══════╗");
  if(cursRow==3){attrset(A_BOLD | A_REVERSE);}else{attrset(A_NORMAL);}
  mvaddstr(21,5,"║ START ║");
  if(cursRow==3){attrset(A_BOLD | A_REVERSE);}else{attrset(A_NORMAL);}
  mvaddstr(22,5,"╚═══════╝");
  refresh();
}

// Re-Implements kbhit from conio.h without having to import conio.h 
// I don't pretend to understand how this works.
// https://web.archive.org/web/20180401093525/http://cc.byexamples.com/2007/04/08/non-blocking-user-input-in-loop-without-ncurses/
int kbhit(){
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}

// Reads ASCII art from file, prints at given horizontal location
void printAscii(int number, int location){
  // setlocale in order to support extended ascii
  setlocale(LC_ALL, "");
  char line[10][64];
  char fname[21];
  sprintf(fname, "numbers/%d.ascii",number);
  FILE *fptr = NULL;
  int i = 0;
  int tot = 0;
  fptr = fopen(fname, "r");
  while(fgets(line[i], 64, fptr)) {
    line[i][strlen(line[i]) - 1] = '\0';
    i++;
  }
  tot = i;
  for(i = 0; i < tot; ++i)
  {
    char stringNum[64];
    sprintf(stringNum,"%s",line[i]);
    attrset(A_NORMAL);
    // Clear character before printing new character. Assumes 10char width
    /*if(number==88){
      mvaddstr((3+i),(5),stringNum);
    }else{*/
    mvaddstr((20+i),(18+location),"          ");
    mvaddstr((20+i),(18+location),stringNum);
    //}
  }
  return;
}

// Takes the totalSeconds from main, and calculates each digit of timer.
// Calls printAscii with char type and location.
int timePrint(unsigned int totalSeconds){
  setlocale (LC_ALL, "");
  attrset(A_BOLD | A_REVERSE);
  mvaddstr(21,5,"║ PAUSE ║");
  while(!kbhit()){
    int timeMin = totalSeconds / 60;
    int timeSec = totalSeconds % 60;

    int minChar1 = timeMin / pow(10, 1);
    minChar1 = minChar1 % 10;
    int minChar2 = timeMin / pow(10, 0);
    minChar2 = minChar2 % 10;

    int secChar1 = timeSec / pow(10, 1);
    secChar1 = secChar1 % 10;
    int secChar2 = timeSec / pow(10, 0);
    secChar2 = secChar2 % 10;

    printAscii(minChar1,0);
    printAscii(minChar2,10);
    printAscii(99,19);
    printAscii(secChar1,24);
    printAscii(secChar2,34);

    refresh();
    sleep(1);
    totalSeconds--;
    // If 0, apply different logic.
    // Done to prevent core dump because math is silly.
    if(totalSeconds==0){
      printAscii(0,34);
      refresh();
      sleep(1);
      totalSeconds = 69420; // Unique number which will never be given by main.
      break;
    }
  }
  
  mvaddstr(21,5,"║ START ║");
  return totalSeconds;
}

// Creates a menu for selecting Study and Break time. Does not return values
// Purely aethetic, logic still works without this method.
// Uses unsigned ints because whatever idk. Doesn't rlly change much.
void timerMenu(unsigned int cursRow, unsigned int myRow, unsigned int cursCol, 
               unsigned int rowOffset, unsigned int colOffset, 
               char typeString[12]){
  // Build Study Time Menu
  if(cursRow==myRow){
  attrset(A_BOLD | A_REVERSE);
  }else{
  attrset(A_NORMAL);
  }
  mvaddstr(rowOffset + 11,0,typeString);
  attrset(A_NORMAL);
  mvaddstr(rowOffset + 12,colOffset+5,"25 Minutes");
  attrset(A_NORMAL);
  mvaddstr(rowOffset + 12,colOffset+20,"15 Minutes");
  attrset(A_NORMAL);
  mvaddstr(rowOffset + 12,colOffset+35,"10 Minutes");
  attrset(A_NORMAL);
  mvaddstr(rowOffset + 12,colOffset+50,"05 Minutes");

  init_pair(1, COLOR_WHITE, COLOR_WHITE);
  switch(cursCol){// No default because I like living on the edge.
    case 1:
      attron(A_BOLD| A_REVERSE);
      mvaddstr(rowOffset + 12,colOffset+5,"25 Minutes");
      refresh();
      break;
    case 2:
      attron(A_BOLD | A_REVERSE);
      mvaddstr(rowOffset + 12,colOffset+20,"15 Minutes");
      refresh();
      break;
    case 3:
      attron(A_BOLD | A_REVERSE);
      mvaddstr(rowOffset + 12,colOffset+35,"10 Minutes");
      refresh();
      break;
    case 4:
      attron(A_BOLD | A_REVERSE);
      mvaddstr(rowOffset + 12,colOffset+50,"05 Minutes");
    default:
      break;
  }
}

// clears the screen, If user exits in non-conventional way (hard to do),
// their terminal will be weird and fucky. This is not good. I don't care.
void clrscr(){
	initscr();
	erase();
	noecho();
	raw();
	move(0,0);
	// Cursor off
	curs_set(0);
	refresh();
}


int main(void){
    // Support for extended ascii because C is too damn old
    setlocale (LC_ALL, "");
  
    // I don't think I have enough variables
    int key;
    int cursCol1 = 1;
    int cursCol2 = 1;
    int cursRow = 1;
    int studyTime = 1500;
    int breakTime = 1500;
    bool timerType = false;
    int returnedTime = 0;
    clrscr();
    // Didn't want to create a fancy method to print this, so yeah.
    // Also, stretches over 80chars, which makes me angry.
    //printAscii(88,1);
    mvaddstr(3,5,"██████╗  ██████╗ ███╗   ███╗ ██████╗ ██████╗  ██████╗ ██████╗  ██████╗ ");
    mvaddstr(4,5,"██╔══██╗██╔═══██╗████╗ ████║██╔═══██╗██╔══██╗██╔═══██╗██╔══██╗██╔═══██╗");
    mvaddstr(5,5,"██████╔╝██║   ██║██╔████╔██║██║   ██║██║  ██║██║   ██║██████╔╝██║   ██║");
    mvaddstr(6,5,"██╔═══╝ ██║   ██║██║╚██╔╝██║██║   ██║██║  ██║██║   ██║██╔══██╗██║   ██║");
    mvaddstr(7,5,"██║     ╚██████╔╝██║ ╚═╝ ██║╚██████╔╝██████╔╝╚██████╔╝██║  ██║╚██████╔╝");
    mvaddstr(8,5,"╚═╝      ╚═════╝ ╚═╝     ╚═╝ ╚═════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ");
    
    refresh();
    timerButton(1);
    timerMenu(1, 1, 1, 0, 8, "Study Time:");
    timerMenu(1, 2, 1, 5, 8, "Break Time:");
    
    //Awaits a 'q' to quit program
    while ((key = getch()) !='q'){
      // Items in each row. Not sure why I did this when I have columns as well.
      int items = 4;
      switch (key){
        case 'd':
          if(cursCol1<items && cursRow==1){
            cursCol1++;
          }else if(cursCol1==items && cursRow==1){
            cursCol1=1;
          }else if(cursCol2<items && cursRow==2){
            cursCol2++;
          }else if(cursCol2==items && cursRow==2){
            cursCol2=1;
          }

          if(cursRow!=3){
            timerType=false;
            returnedTime=0;
          }
          break;
        case 'a':
          if(cursCol1==1 && cursRow==1){
            cursCol1=items;
          }else if(cursCol1>1 && cursRow==1){
            cursCol1--;
          }else if(cursCol2==1 && cursRow==2){
            cursCol2=items;
          }else if(cursCol2>1 && cursRow==2){
            cursCol2--;
          }

          if(cursRow!=3){
            timerType=false;
            returnedTime=0;
          }
          break;
        case 's':
          if(cursRow==1){
            cursRow=2;
          }
          else if(cursRow==2){
            cursRow=3;
          }else if(cursRow==3){
            cursRow=1;
          }
          break;
        case 'w':
          if(cursRow==1){
            cursRow=3;
          }else if(cursRow==2){
            cursRow=1;
          }else if(cursRow==3){
            cursRow=2;
          }
          break;
        // This is some of the most disgusting logic I have ever written
        case 'e':
          if(cursRow==3){
            if(returnedTime!=0){
              if(timerType == false){
                studyTime = returnedTime;
              }else{
                breakTime = returnedTime;
              }
            }
            char key2 = 'e';
            if(timerType==true && returnedTime!=0 && returnedTime!=69420){
              key2 = getch();
            }else if(returnedTime!=0 && returnedTime !=69420){
              key2 = getch();
            }
            if(key2=='e'){
              if(timerType == false){
                attrset(A_BOLD | A_REVERSE);
                mvaddstr(24,3,"|| STUDYIN ||");
                returnedTime = timePrint(studyTime);
              }else if(timerType == true){
                attrset(A_BOLD | A_REVERSE);
                mvaddstr(24,3,"|| CHILLIN ||");
                returnedTime = timePrint(breakTime);
              }
              if(returnedTime == 0){
                if(timerType==true){
                  timerType=false;
                }else{
                  timerType=true;
                }
              }
            }
          }
          break;
        default:
          break;
      }
      // Loop the countdowns until a user input. Sometimes works, sometime not.
      while(returnedTime==69420){
        if(timerType==false){
          timerType=true;
          returnedTime=0;
          attrset(A_BOLD | A_REVERSE);
          mvaddstr(24,3,"|| CHILLIN ||");
          returnedTime=timePrint(breakTime);
        }else if(timerType==true){
          timerType=false;
          returnedTime=0;
          attrset(A_BOLD | A_REVERSE);
          mvaddstr(24,3,"|| STUDYIN ||");
          returnedTime=timePrint(studyTime);
        }
      }
      // Wrote this switch twice because I am tired.
      switch(cursCol1){
        case 1:
          studyTime = 1500;
          break;
        case 2:
          studyTime = 900;
          break;
        case 3:
          studyTime = 600;
          break;
        case 4:
          //studyTime = 10;
          studyTime = 300;
          break;
      }

      switch(cursCol2){
        case 1:
          breakTime = 1500;
          break;
        case 2:
          breakTime = 900;
          break;
        case 3:
          breakTime = 600;
          break;
        case 4:
          //breakTime = 10; // Testing or sumthin
          breakTime = 300;
          break;
      }

      // Build Study Time Menu
      timerMenu(cursRow, 1, cursCol1, 0, 8, "Study Time:");
      timerMenu(cursRow, 2, cursCol2, 5, 8, "Break Time:");
      timerButton(cursRow);

  
    }    

    refresh();
    // Fix the term before kicking you out
    getch();
    echo();
    endwin();
    return EXIT_SUCCESS;
}
//EOF
